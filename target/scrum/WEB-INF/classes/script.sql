#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Etat
#------------------------------------------------------------

CREATE TABLE Etat(
        idEtat  Int  Auto_increment  NOT NULL ,
        libelle Varchar (50) NOT NULL
	,CONSTRAINT Etat_PK PRIMARY KEY (idEtat)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Type
#------------------------------------------------------------

CREATE TABLE Type(
        idType   Int  Auto_increment  NOT NULL ,
        intitule Varchar (50) NOT NULL ,
        couleur  Varchar (50) NOT NULL
	,CONSTRAINT Type_PK PRIMARY KEY (idType)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Sprint
#------------------------------------------------------------

CREATE TABLE Sprint(
        idSprint  Int  Auto_increment  NOT NULL ,
        nom       Varchar (50) NOT NULL ,
        dateDebut Date NOT NULL ,
        dateFin   Date
	,CONSTRAINT Sprint_PK PRIMARY KEY (idSprint)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Developpeur
#------------------------------------------------------------

CREATE TABLE Developpeur(
        idDeveloppeur Int  Auto_increment  NOT NULL ,
        nom           Varchar (50) NOT NULL ,
        prenom        Varchar (50) NOT NULL ,
        motDePasse    Varchar (50) NOT NULL ,
        estAdmin      Bool NOT NULL
	,CONSTRAINT Developpeur_PK PRIMARY KEY (idDeveloppeur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tache
#------------------------------------------------------------

CREATE TABLE Tache(
        idTache       Int  Auto_increment  NOT NULL ,
        intitule      Varchar (50) NOT NULL ,
        idType        Int NOT NULL ,
        idSprint      Int NOT NULL ,
        idDeveloppeur Int
	,CONSTRAINT Tache_PK PRIMARY KEY (idTache)

	,CONSTRAINT Tache_Type_FK FOREIGN KEY (idType) REFERENCES Type(idType)
	,CONSTRAINT Tache_Sprint0_FK FOREIGN KEY (idSprint) REFERENCES Sprint(idSprint)
	,CONSTRAINT Tache_Developpeur1_FK FOREIGN KEY (idDeveloppeur) REFERENCES Developpeur(idDeveloppeur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Changer
#------------------------------------------------------------

CREATE TABLE Changer(
        idEtat    Int NOT NULL ,
        idTache   Int NOT NULL ,
        dateDebut Date NOT NULL ,
        dateFin   Date
	,CONSTRAINT Changer_PK PRIMARY KEY (idEtat,idTache)

	,CONSTRAINT Changer_Etat_FK FOREIGN KEY (idEtat) REFERENCES Etat(idEtat)
	,CONSTRAINT Changer_Tache0_FK FOREIGN KEY (idTache) REFERENCES Tache(idTache)
)ENGINE=InnoDB;

