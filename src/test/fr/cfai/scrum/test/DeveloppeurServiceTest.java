package fr.cfai.scrum.test;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.service.DeveloppeurService;
import fr.cfai.scrum.service.impl.DeveloppeurServiceImpl;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DeveloppeurServiceTest extends TestCase {

	private DeveloppeurService developpeurService = null;
	private static Developpeur developpeurTest = null;

	public DeveloppeurServiceTest(String nom) {
		super(nom);
		developpeurService = new DeveloppeurServiceImpl();
	}

	public void testerAjouterDeveloppeur() {

		developpeurTest = developpeurService.ajouterDeveloppeur("TestNom", "Testprenom", "TestmotDePasse", false);
		assertNotNull(developpeurTest);
		assertTrue(developpeurTest.getNomDeveloppeur().equals("TestNom"));
		assertTrue(developpeurTest.getPrenomDeveloppeur().equals("Testprenom"));
		assertTrue(developpeurTest.getMotDePasse().equals("TestmotDePasse"));
		assertTrue(developpeurTest.isEstAdmin() == false);
	}

	public void testerRecupererDeveloppeurParId() {
		developpeurTest = developpeurService.recupererDeveloppeurParId(developpeurTest.getIdDeveloppeur());
		assertNotNull(developpeurTest);
		assertTrue(developpeurTest.getNomDeveloppeur().equals("TestNom"));
		assertTrue(developpeurTest.getPrenomDeveloppeur().equals("Testprenom"));
		assertTrue(developpeurTest.getMotDePasse().equals("TestmotDePasse"));
		assertTrue(developpeurTest.isEstAdmin() == false);
	}

	public void testerRecupererDeveloppeurs() {
		assertTrue(!developpeurService.recupererDeveloppeurs().isEmpty());
	}

	public void testerModifierDeveloppeur() {
		developpeurTest.setNomDeveloppeur("TestNomModif");
		developpeurTest.setPrenomDeveloppeur("TestprenomModif");
		developpeurTest.setMotDePasse("TestmotDePasseModif");
		developpeurTest.setEstAdmin(true);
		developpeurService.modifierDeveloppeur(developpeurTest);
		assertNotNull(developpeurTest);
		assertTrue(developpeurTest.getNomDeveloppeur().equals("TestNomModif"));
		assertTrue(developpeurTest.getPrenomDeveloppeur().equals("TestprenomModif"));
		assertTrue(developpeurTest.getMotDePasse().equals("TestmotDePasseModif"));
		assertTrue(developpeurTest.isEstAdmin() == true);
	}

	public void testerDeleteDeveloppeur() {
		developpeurService.effacerDeveloppeur(developpeurTest.getIdDeveloppeur());
		assertNull(developpeurService.recupererDeveloppeurParId(developpeurTest.getIdDeveloppeur()));

	}

	public static Test suite() {
		TestSuite testSuite = new TestSuite();

		System.out.println("Lancement de la suite de test sur DeveloppeurService");

		testSuite.addTest(new DeveloppeurServiceTest("testerAjouterDeveloppeur"));
		testSuite.addTest(new DeveloppeurServiceTest("testerRecupererDeveloppeurParId"));
		testSuite.addTest(new DeveloppeurServiceTest("testerRecupererDeveloppeurs"));
		testSuite.addTest(new DeveloppeurServiceTest("testerModifierDeveloppeur"));
		testSuite.addTest(new DeveloppeurServiceTest("testerDeleteDeveloppeur"));
		return testSuite;
	}

}
