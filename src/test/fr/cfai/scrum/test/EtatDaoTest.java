package fr.cfai.scrum.test;

import java.sql.SQLException;

import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.dao.EtatDao;
import fr.cfai.scrum.dao.impl.EtatDaoImpl;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class EtatDaoTest extends TestCase {
	private EtatDao etatDao = null;
	private static Etat etatTest = null;

	public EtatDaoTest(String nom) {
		super(nom);
		etatDao = new EtatDaoImpl();
	}

	public void testerCreate() {
		Etat etat = null;
		etatTest = new Etat();
		etatTest.setLibelle("Test");
		try {
			etat = etatDao.create(etatTest);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertNotNull(etat);
		assertTrue(etat.getLibelle().equals("Test"));

	}

	public void testerFindAll() {
		try {
			assertTrue(!etatDao.findAll().isEmpty());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void testerFindEtatById() {
		Etat etat = new Etat();
		try {
			etat = etatDao.findEtatById(etatTest.getIdEtat());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertNotNull(etat);
		assertTrue(etat.getLibelle().equals("Test"));

	}

	public void testerUpdate() {
		Etat etat = new Etat();
		etat = null;
		etatTest.setLibelle("TestModifier");
		try {
			etatDao.update(etatTest);
			etat = etatDao.findEtatById(etatTest.getIdEtat());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertTrue(etat.getLibelle().equals("TestModifier"));

	}

	public void testerDelete() {
		int idEtatTest = etatTest.getIdEtat();
		try {
			etatDao.delete(idEtatTest);
			assertNull(etatDao.findEtatById(idEtatTest));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static Test suite() {
		TestSuite testSuite = new TestSuite();

		System.out.println("Lancement de la suite de test sur EtatDao");

		testSuite.addTest(new EtatDaoTest("testerCreate"));
		testSuite.addTest(new EtatDaoTest("testerFindAll"));
		testSuite.addTest(new EtatDaoTest("testerFindEtatById"));
		testSuite.addTest(new EtatDaoTest("testerUpdate"));
		testSuite.addTest(new EtatDaoTest("testerDelete"));
		return testSuite;
	}

}
