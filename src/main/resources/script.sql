/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : scrum

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 09/04/2019 16:42:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for changer
-- ----------------------------
DROP TABLE IF EXISTS `changer`;
CREATE TABLE `changer`  (
  `idEtat` int(11) NOT NULL,
  `idTache` int(11) NOT NULL,
  `dateDebut` timestamp(0) NOT NULL,
  `dateFin` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idEtat`, `idTache`,`dateDebut`) USING BTREE,
  INDEX `Changer_Tache0_FK`(`idTache`) USING BTREE,
  CONSTRAINT `Changer_Etat_FK` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`idEtat`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Changer_Tache0_FK` FOREIGN KEY (`idTache`) REFERENCES `tache` (`idTache`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for developpeur
-- ----------------------------
DROP TABLE IF EXISTS `developpeur`;
CREATE TABLE `developpeur`  (
  `idDeveloppeur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `prenom` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `motDePasse` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `estAdmin` tinyint(1) NOT NULL,
  PRIMARY KEY (`idDeveloppeur`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for etat
-- ----------------------------
DROP TABLE IF EXISTS `etat`;
CREATE TABLE `etat`  (
  `idEtat` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`idEtat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sprint
-- ----------------------------
DROP TABLE IF EXISTS `sprint`;
CREATE TABLE `sprint`  (
  `idSprint` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NULL DEFAULT NULL,
  `isActive` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idSprint`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tache
-- ----------------------------
DROP TABLE IF EXISTS `tache`;
CREATE TABLE `tache`  (
  `idTache` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idType` int(11) NOT NULL,
  `idSprint` int(11) NOT NULL,
  `idDeveloppeur` int(11) NULL DEFAULT NULL,
  `idEtat` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idTache`) USING BTREE,
  INDEX `Tache_Type_FK`(`idType`) USING BTREE,
  INDEX `Tache_Sprint0_FK`(`idSprint`) USING BTREE,
  INDEX `Tache_Developpeur1_FK`(`idDeveloppeur`) USING BTREE,
  INDEX `Tache_Etat_FK`(`idEtat`) USING BTREE,
  CONSTRAINT `Tache_Developpeur1_FK` FOREIGN KEY (`idDeveloppeur`) REFERENCES `developpeur` (`idDeveloppeur`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Tache_Etat_FK` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`idEtat`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Tache_Sprint0_FK` FOREIGN KEY (`idSprint`) REFERENCES `sprint` (`idSprint`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Tache_Type_FK` FOREIGN KEY (`idType`) REFERENCES `type` (`idType`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `idType` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `couleur` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`idType`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
