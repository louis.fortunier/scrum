package fr.cfai.scrum.business;

import java.util.List;

public class Tache {

	private int idTache;
	private String intitule;
	private List<ChangementEtat> changementsEtats;
	private Developpeur developpeur;
	private Type type;
	private Sprint sprint;
	private Etat etat;

	public Tache() {
		// TODO Auto-generated constructor stub
	}

	public int getIdTache() {
		return idTache;
	}

	public void setIdTache(int idTache) {
		this.idTache = idTache;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public List<ChangementEtat> getChangementsEtats() {
		return changementsEtats;
	}

	public void setChangementsEtats(List<ChangementEtat> changementsEtats) {
		this.changementsEtats = changementsEtats;
	}

	public Developpeur getDeveloppeur() {
		return developpeur;
	}

	public void setDeveloppeur(Developpeur developpeur) {
		this.developpeur = developpeur;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}
	
	public Etat getEtat() {
		return etat;
	}
	
	public void setEtat(Etat etat) {
		this.etat = etat;
	}

	@Override
	public String toString() {
		return "Tache [idTache=" + idTache + ", intitule=" + intitule + ", changementsEtats=" + changementsEtats
				+ ", developpeur=" + developpeur + ", type=" + type + ", sprint=" + sprint + "]";
	}

}
