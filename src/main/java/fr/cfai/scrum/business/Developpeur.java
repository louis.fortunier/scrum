package fr.cfai.scrum.business;

import java.util.List;

public class Developpeur {
	private int idDeveloppeur;
	private String nom;
	private String prenom;
	private String motDePasse;
	private boolean estAdmin;
	private List<Tache> taches;

	public Developpeur() {
		// TODO Auto-generated constructor stub
	}
	
	public Developpeur(String nom, String mdp) {
		super();
		this.nom = nom;
		this.motDePasse = mdp;
	}	

	public int getIdDeveloppeur() {
		return idDeveloppeur;
	}

	public void setIdDeveloppeur(int idDeveloppeur) {
		this.idDeveloppeur = idDeveloppeur;
	}

	public String getNomDeveloppeur() {
		return nom;
	}

	public void setNomDeveloppeur(String nomDeveloppeur) {
		this.nom = nomDeveloppeur;
	}

	public String getPrenomDeveloppeur() {
		return prenom;
	}

	public void setPrenomDeveloppeur(String prenomDeveloppeur) {
		this.prenom = prenomDeveloppeur;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public boolean isEstAdmin() {
		return estAdmin;
	}

	public void setEstAdmin(boolean estAdmin) {
		this.estAdmin = estAdmin;
	}

	public List<Tache> getTaches() {
		return taches;
	}

	public void setTaches(List<Tache> taches) {
		this.taches = taches;
	}

	@Override
	public String toString() {
		return "Developpeur [idDeveloppeur=" + idDeveloppeur + ", nomDeveloppeur=" + nom
				+ ", prenomDeveloppeur=" + prenom + ", motDePasse=" + motDePasse + ", estAdmin=" + estAdmin
				+ ", taches=" + taches + "]";
	}

}
