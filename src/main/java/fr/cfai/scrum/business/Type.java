package fr.cfai.scrum.business;

import java.util.List;

public class Type {
	private int idType;
	private String intitule;
	private String couleur;
	private List<Tache> taches;
	
	public Type() {
		// TODO Auto-generated constructor stub
	}

	public int getIdType() {
		return idType;
	}

	public void setIdType(int idType) {
		this.idType = idType;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public List<Tache> getTaches() {
		return taches;
	}

	public void setTaches(List<Tache> taches) {
		this.taches = taches;
	}

	@Override
	public String toString() {
		return "Type [idType=" + idType + ", intitule=" + intitule + ", couleur=" + couleur + ", taches=" + taches
				+ "]";
	}
	
	

}
