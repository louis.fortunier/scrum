package fr.cfai.scrum.business;

import java.util.Date;

public class ChangementEtat {

	private Etat etat;
	private Tache tache;
	private Date dateDebut;
	private Date dateFin;

	public ChangementEtat() {
		// TODO Auto-generated constructor stub
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	
	
	public Tache getTache() {
		return tache;
	}

	public void setTache(Tache tache) {
		this.tache = tache;
	}

	@Override
	public String toString() {
		return "ChangementEtat [ etat=" + etat + ", tache=" + tache
				+ ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + "]";
	}
	
}
