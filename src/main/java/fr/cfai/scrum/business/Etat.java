package fr.cfai.scrum.business;

import java.util.List;

public class Etat {
	
	private int idEtat;
	private String libelle;
	private List<ChangementEtat> changementsEtats;	
	
	public Etat() {
		
	}


	public int getIdEtat() {
		return idEtat;
	}


	public void setIdEtat(int idEtat) {
		this.idEtat = idEtat;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public List<ChangementEtat> getChangementsEtats() {
		return changementsEtats;
	}


	public void setChangementsEtats(List<ChangementEtat> changementsEtats) {
		this.changementsEtats = changementsEtats;
	}

	@Override
	public String toString() {
		return "Etat [idEtat=" + idEtat + ", libelle=" + libelle + ", changementsEtats=" + changementsEtats + "]";
	}


		
	

}
