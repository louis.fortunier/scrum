package fr.cfai.scrum;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.service.DeveloppeurService;
import fr.cfai.scrum.service.impl.DeveloppeurServiceImpl;


public class App {

	public static void main(String[] args) {
		DeveloppeurService developpeurService = new DeveloppeurServiceImpl();
		Developpeur dev = developpeurService.verifierDeveloppeur("admin", "admin", "12345");
		if (dev == null) {
			developpeurService.ajouterDeveloppeur("admin", "admin", "12345", true);
		}
	}

}
