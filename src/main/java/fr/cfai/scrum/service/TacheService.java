package fr.cfai.scrum.service;

import java.util.List;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.business.Sprint;
import fr.cfai.scrum.business.Tache;
import fr.cfai.scrum.business.Type;

public interface TacheService {
	public Tache ajouterTache(String intitule,Type type,Sprint sprint,Developpeur developpeur, Etat etat);
	public List<Tache> recupererTaches();
	public Tache recupererTacheParId(int idTache);
	public Tache modifierTache(Tache tache);
	public void modifierEtatTache(int idTache , int idEtat);
	public void effacerTache(int idTache);

}
