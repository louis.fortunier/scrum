package fr.cfai.scrum.service;

import java.util.Date;
import java.util.List;

import fr.cfai.scrum.business.ChangementEtat;
import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.business.Tache;

public interface ChangementEtatService {

	public ChangementEtat ajouterChangementEtat(Date dateDebut,Etat etat , Tache tache );
	public List<ChangementEtat> recupererChangementEtat();
	public List<ChangementEtat> recupererChangementEtatParTacheId(int idTache);
	public ChangementEtat recupererChangementEtatParDateFinNullEtTacheId(int idTache);
	public ChangementEtat modifierChangementEtat(ChangementEtat changementEtat);
	public void effacerChangementEtat(int idChangementEtat);
}
