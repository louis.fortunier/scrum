package fr.cfai.scrum.service.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import fr.cfai.scrum.business.ChangementEtat;
import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.business.Tache;
import fr.cfai.scrum.dao.ChangementEtatDao;
import fr.cfai.scrum.dao.impl.ChangementEtatDaoImpl;
import fr.cfai.scrum.service.ChangementEtatService;

public class ChangementEtatSreviceImpl implements ChangementEtatService {

	ChangementEtatDao ced = new ChangementEtatDaoImpl();

	@Override
	public ChangementEtat ajouterChangementEtat(Date dateDebut, Etat etat, Tache tache) {

		ChangementEtat changementEtat = recupererChangementEtatParDateFinNullEtTacheId(tache.getIdTache());

		if (changementEtat == null
				|| (changementEtat.getDateFin() == null && changementEtat.getEtat().getIdEtat() != etat.getIdEtat())) {
			ChangementEtat nouveauChangementEtat = new ChangementEtat();
			nouveauChangementEtat.setDateDebut(dateDebut);
			nouveauChangementEtat.setEtat(etat);
			nouveauChangementEtat.setTache(tache);

			if (changementEtat != null) {
				changementEtat.setDateFin(new Date());
				modifierChangementEtat(changementEtat);
			}

			try {
				changementEtat = ced.create(nouveauChangementEtat);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return changementEtat;
		}

		return null;

	}

	@Override
	public List<ChangementEtat> recupererChangementEtat() {
		try {
			return ced.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public List<ChangementEtat> recupererChangementEtatParTacheId(int idTache) {
		try {
			return ced.findChangementEtatByTacheId(idTache);
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public ChangementEtat recupererChangementEtatParDateFinNullEtTacheId(int idTache) {
		try {
			return ced.findChangementEtatByDateFinNullAndTacheId(idTache);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ChangementEtat modifierChangementEtat(ChangementEtat changementEtat) {
		try {
			return ced.update(changementEtat);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void effacerChangementEtat(int idChangementEtat) {
		try {
			ced.delete(idChangementEtat);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
