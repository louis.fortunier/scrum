package fr.cfai.scrum.service.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.dao.DeveloppeurDao;
import fr.cfai.scrum.dao.impl.DeveloppeurDaoImpl;
import fr.cfai.scrum.service.DeveloppeurService;

public class DeveloppeurServiceImpl implements DeveloppeurService {

	private DeveloppeurDao dd = new DeveloppeurDaoImpl(); 
	
	@Override
	public Developpeur ajouterDeveloppeur(String nom, String prenom, String motDePasse, boolean estAdmin) {
		Developpeur nouveauDeveloppeur = new Developpeur();
		nouveauDeveloppeur.setNomDeveloppeur(nom);
		nouveauDeveloppeur.setPrenomDeveloppeur(prenom);
		nouveauDeveloppeur.setMotDePasse(motDePasse);
		nouveauDeveloppeur.setEstAdmin(estAdmin);
		Developpeur developpeur = null;
		
		try {
			developpeur = dd.create(nouveauDeveloppeur);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return developpeur;
	}

	@Override
	public List<Developpeur> recupererDeveloppeurs() {
		try {
			return dd.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public Developpeur recupererDeveloppeurParId(int idDeveloppeur) {
		try {
			return dd.findDeveloppeurById(idDeveloppeur);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public Developpeur modifierDeveloppeur(Developpeur developpeur) {
		try {
			return dd.update(developpeur);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void effacerDeveloppeur(int idDeveloppeur) {
		try {
			dd.delete(idDeveloppeur);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	// faire une méthode vérifierdeveloppeur qui va vérifié si le developpeur existe dans la base de donnée
	
	public Developpeur verifierDeveloppeur(String nom, String prenom, String motDePasse) {
		try {
			return dd.findDeveloppeurByNomPrenomMotDePasse(nom, prenom, motDePasse);
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Developpeur recupererDeveloppeurParNomEtMotDePasse(String nom, String mdp) {
		Developpeur developpeur = null;
		
		try {
			developpeur = dd.findByNomAndMotDePasse(nom, mdp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return developpeur;
	}
}
