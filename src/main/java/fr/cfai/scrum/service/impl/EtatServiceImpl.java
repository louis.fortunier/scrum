package fr.cfai.scrum.service.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.dao.EtatDao;
import fr.cfai.scrum.dao.impl.EtatDaoImpl;
import fr.cfai.scrum.service.EtatService;

public class EtatServiceImpl implements EtatService {
	
	EtatDao ed = new EtatDaoImpl();

	@Override
	public Etat ajouterEtat(String libelle) {
		Etat nouveauEtat = new Etat();
		nouveauEtat.setLibelle(libelle);
		
		Etat etat = null;
		
		try {
			etat = ed.create(nouveauEtat);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return etat;
	}

	@Override
	public List<Etat> recupererEtat() {
		try {
			return ed.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public Etat recupererEtatParId(int idEtat) {
		try {
			return ed.findEtatById(idEtat);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Etat modifierEtat(Etat etat) {
		try {
			return ed.update(etat);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void effacerEtat(int idEtat) {
		try {
			ed.delete(idEtat);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Etat verifierEtat(String libelle) {
		try {
			return ed.findEtatByIntitule(libelle);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
