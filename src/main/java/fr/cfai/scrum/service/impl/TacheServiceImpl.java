package fr.cfai.scrum.service.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.business.Sprint;
import fr.cfai.scrum.business.Tache;
import fr.cfai.scrum.business.Type;
import fr.cfai.scrum.dao.TacheDao;
import fr.cfai.scrum.dao.impl.TacheDaoImpl;
import fr.cfai.scrum.service.TacheService;

public class TacheServiceImpl implements TacheService {

	private TacheDao td = new TacheDaoImpl(); 
	
	@Override
	public Tache ajouterTache(String intitule, Type type, Sprint sprint, Developpeur developpeur, Etat etat) {
		Tache nouvelleTache = new Tache();
		nouvelleTache.setIntitule(intitule);
		nouvelleTache.setType(type);
		nouvelleTache.setSprint(sprint);
		nouvelleTache.setDeveloppeur(developpeur);
		nouvelleTache.setEtat(etat);
		Tache tache = null;
		try {
			tache = td.create(nouvelleTache);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tache;
	}

	@Override
	public List<Tache> recupererTaches() {
		try {
			return td.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public Tache recupererTacheParId(int idTache) {
		try {
			return td.findTacheById(idTache);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Tache modifierTache(Tache tache) {
		try {
			return td.update(tache);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void modifierEtatTache(int idTache, int idEtat) {
		try {
			td.updateEtat(idTache, idEtat);
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void effacerTache(int idTache) {
		try {
			td.delete(idTache);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
