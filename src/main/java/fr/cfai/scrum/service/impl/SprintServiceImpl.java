package fr.cfai.scrum.service.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import fr.cfai.scrum.business.Sprint;
import fr.cfai.scrum.dao.SprintDao;
import fr.cfai.scrum.dao.impl.SprintDaoImpl;
import fr.cfai.scrum.service.SprintService;

public class SprintServiceImpl implements SprintService{

	SprintDao sd = new SprintDaoImpl();
	
	@Override
	public Sprint ajouterSprint(String nom, Date dateDebut , Date dateFin, boolean isActive) {
		Sprint nouveauSprint = new Sprint();
		nouveauSprint.setNom(nom);
		nouveauSprint.setDateDebut(dateDebut);
		nouveauSprint.setDateFin(dateFin);
		nouveauSprint.setActive(isActive);
		
		Sprint sprint = null;
		
		try {
			sprint = sd.create(nouveauSprint);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sprint;
		
	}

	@Override
	public List<Sprint> recupererSprint() {
		try {
			return sd.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public Sprint recupererSprintParId(int idSprint) {
		try {
			return sd.findSprintById(idSprint);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Sprint recupererSprintParNom(String nom) {
		try {
			return sd.findSprintByNom(nom);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Sprint recupererSprintActif() {
		try {
			return sd.findSprintActif();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Sprint modifierSprint(Sprint sprint) {
		try {
			return sd.update(sprint);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void effacerSprint(int idSprint) {
		try {
			sd.delete(idSprint);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
