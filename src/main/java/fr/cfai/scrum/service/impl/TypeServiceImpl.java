package fr.cfai.scrum.service.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import fr.cfai.scrum.business.Type;
import fr.cfai.scrum.dao.TypeDao;
import fr.cfai.scrum.dao.impl.TypeDaoImpl;
import fr.cfai.scrum.service.TypeService;

public class TypeServiceImpl implements TypeService {

	TypeDao td = new TypeDaoImpl();
	
	@Override
	public Type ajouterType(String intitule, String couleur) {
		Type nouveauType = new Type();
		nouveauType.setIntitule(intitule);
		nouveauType.setCouleur(couleur);
		
		Type type = null;
		
		try {
			type = td.create(nouveauType);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return type;
	}

	@Override
	public List<Type> recupererType() {
		try {
			return td.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	@Override
	public Type recupererTypeParId(int idType) {
		try {
			return td.findTypeById(idType);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Type modifierType(Type type) {
		try {
			return td.update(type);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void effacerType(int idType) {
		try {
			td.delete(idType);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public Type recupererTypeParNomCouleur(String nom, String couleur) {
		try {
			return td.findTypeByNomCouleur(nom, couleur);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
