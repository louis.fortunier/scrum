package fr.cfai.scrum.service;

import java.util.List;

import fr.cfai.scrum.business.Developpeur;

public interface DeveloppeurService {
	public Developpeur ajouterDeveloppeur(String nom,String prenom,String motDePasse,boolean estAdmin);
	public List<Developpeur> recupererDeveloppeurs();
	public Developpeur recupererDeveloppeurParId(int idDeveloppeur);
	public Developpeur recupererDeveloppeurParNomEtMotDePasse(String nom, String mdp);
	public Developpeur verifierDeveloppeur(String nom, String prenom, String motDePasse);
	public Developpeur modifierDeveloppeur(Developpeur developpeur);
	public void effacerDeveloppeur(int idDeveloppeur);
}
