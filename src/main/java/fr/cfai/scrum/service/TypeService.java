package fr.cfai.scrum.service;

import java.util.List;

import fr.cfai.scrum.business.Type;

public interface TypeService {

	public Type ajouterType(String intitule , String couleur );
	public List<Type> recupererType();
	public Type recupererTypeParId(int idType);
	public Type recupererTypeParNomCouleur(String nom, String couleur);
	public Type modifierType(Type type);
	public void effacerType(int idType);
}
