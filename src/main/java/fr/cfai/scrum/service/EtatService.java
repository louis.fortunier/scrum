package fr.cfai.scrum.service;

import java.util.List;

import fr.cfai.scrum.business.Etat;

public interface EtatService {

	public Etat ajouterEtat(String libelle);
	public List<Etat> recupererEtat();
	public Etat recupererEtatParId(int idEtat);
	public Etat verifierEtat(String libelle);
	public Etat modifierEtat(Etat etat);
	public void effacerEtat(int idEtat);
}
