package fr.cfai.scrum.service;

import java.util.Date;
import java.util.List;

import fr.cfai.scrum.business.Sprint;

public interface SprintService {
	
	public Sprint ajouterSprint(String nom, Date dateDebut , Date dateFin, boolean isActive);
	public List<Sprint> recupererSprint();
	public Sprint recupererSprintParId(int idSprint);
	public Sprint recupererSprintParNom(String nom);
	public Sprint recupererSprintActif();
	public Sprint modifierSprint(Sprint sprint);
	public void effacerSprint(int idSprint);
}
