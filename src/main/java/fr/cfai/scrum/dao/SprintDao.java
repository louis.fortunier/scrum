package fr.cfai.scrum.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cfai.scrum.business.Sprint;

public interface SprintDao {

	public Sprint create(Sprint sprint) throws SQLException;
	
	public List<Sprint> findAll() throws SQLException;
	
	public Sprint findSprintById(int idSprint) throws SQLException;
	
	public Sprint findSprintByNom(String nom) throws SQLException;
	
	public Sprint findSprintActif() throws SQLException;
	
	public void delete(int idSprint) throws SQLException;
	
	public Sprint update(Sprint Sprint) throws SQLException;
}
