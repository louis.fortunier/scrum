package fr.cfai.scrum.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cfai.scrum.business.Etat;

public interface EtatDao {
	
	public Etat create(Etat etat) throws SQLException;
	
	public List<Etat> findAll() throws SQLException;
	
	public Etat findEtatById(int idEtat) throws SQLException;
	
	public void delete(int idEtat) throws SQLException;
	
	public Etat update(Etat etat) throws SQLException;
	
	public Etat findEtatByIntitule(String libelle) throws SQLException;
}
