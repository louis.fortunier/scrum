package fr.cfai.scrum.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cfai.scrum.business.ChangementEtat;

public interface ChangementEtatDao {

	public ChangementEtat create(ChangementEtat changementEtat) throws SQLException;

	public List<ChangementEtat> findAll() throws SQLException;

	public List<ChangementEtat> findChangementEtatByTacheId(int idTache) throws SQLException;

	public ChangementEtat findChangementEtatByDateFinNullAndTacheId(int idTache) throws SQLException;

	public void delete(int idChangementEtat) throws SQLException;

	public ChangementEtat update(ChangementEtat changementEtat) throws SQLException;
}
