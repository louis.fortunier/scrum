package fr.cfai.scrum.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cfai.scrum.business.Tache;

public interface TacheDao {
	
	public Tache create(Tache tache) throws SQLException;
	
	public List<Tache> findAll() throws SQLException;
	
	public Tache findTacheById(int idTache) throws SQLException;
	
	public void delete(int idTache) throws SQLException;
	
	public Tache update(Tache tache) throws SQLException;
	
	public void updateEtat(int idTache, int idEtat) throws SQLException;
}
