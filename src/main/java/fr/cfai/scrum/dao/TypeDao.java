package fr.cfai.scrum.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cfai.scrum.business.Type;

public interface TypeDao {

	public Type create(Type type) throws SQLException;
	
	public List<Type> findAll() throws SQLException;
	
	public Type findTypeById(int idType) throws SQLException;
	
	public Type findTypeByNomCouleur(String nom , String couleur) throws SQLException;
	
	public void delete(int idType) throws SQLException;
	
	public Type update(Type type) throws SQLException;
}
