package fr.cfai.scrum.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.business.Type;
import fr.cfai.scrum.dao.ConnexionBdd;
import fr.cfai.scrum.dao.Requetes;
import fr.cfai.scrum.dao.TypeDao;

public class TypeDaoImpl implements TypeDao {

	private Connection connection;

	public TypeDaoImpl() {
		try {
			this.connection = ConnexionBdd.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	@Override
	public Type create(Type type) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_TYPE,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, type.getIntitule());
		ps.setString(2, type.getCouleur());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		type.setIdType(rs.getInt(1));
		return type;
	}

	@Override
	public List<Type> findAll() throws SQLException {
		List<Type> types = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(Requetes.TOUS_LES_TYPES);
		while (rs.next()) {
			Type type = new Type();
			type.setIdType(rs.getInt(1));
			type.setIntitule(rs.getString(2));
			type.setCouleur(rs.getString(3));
			types.add(type);
		}
		return types;
	}

	@Override
	public Type findTypeById(int idType) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.TYPE_PAR_ID);
		ps.setInt(1,idType);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			Type etat = new Type();
			etat.setIdType(rs.getInt(1));
			etat.setIntitule(rs.getString(2));
			etat.setCouleur(rs.getString(3));
			return etat;
			}
		return null;
	}

	@Override
	public void delete(int idType) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.SUPPRIMER_TYPE);
		statement.setInt(1, idType);
		statement.execute();
	}

	@Override
	public Type update(Type type) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.MODIFIER_TYPE);
		statement.setString(1,type.getIntitule());
		statement.setString(2,type.getCouleur());
		statement.setInt(3,type.getIdType());
		statement.execute();
		return type;
	}

	@Override
	public Type findTypeByNomCouleur(String nom, String couleur) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.TYPE_PAR_NOM_COULEUR);
		ps.setString(1, nom);
		ps.setString(2, couleur);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Type type = new Type();
			type.setIdType(rs.getInt(1));
			type.setIntitule(rs.getString(2));
			type.setCouleur(rs.getString(3));
			return type;
		}
		return null;
	}

}
