package fr.cfai.scrum.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.dao.ConnexionBdd;
import fr.cfai.scrum.dao.DeveloppeurDao;
import fr.cfai.scrum.dao.Requetes;

public class DeveloppeurDaoImpl implements DeveloppeurDao {

	private Connection connection;

	public DeveloppeurDaoImpl() {
		try {
			this.connection = ConnexionBdd.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public Developpeur create(Developpeur developpeur) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_DEVELOPPEUR, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, developpeur.getNomDeveloppeur());
		ps.setString(2, developpeur.getPrenomDeveloppeur());
		ps.setString(3, developpeur.getMotDePasse());
		ps.setBoolean(4, developpeur.isEstAdmin());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		developpeur.setIdDeveloppeur(rs.getInt(1));
		return developpeur;

	}

	@Override
	public List<Developpeur> findAll() throws SQLException {
		// Creation d'une nouvelle liste d'etats
		List<Developpeur> developpeurs = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(Requetes.TOUS_LES_DEVELOPPEURS);
		while (rs.next()) {
			Developpeur developpeur = new Developpeur();
			developpeur.setIdDeveloppeur(rs.getInt(1));
			developpeur.setNomDeveloppeur(rs.getString(2));
			developpeur.setPrenomDeveloppeur(rs.getString(3));
			developpeur.setMotDePasse(rs.getString(4));
			developpeur.setEstAdmin(rs.getBoolean(5));
			developpeurs.add(developpeur);
		}
		return developpeurs;
	}

	@Override
	public Developpeur findDeveloppeurById(int idDeveloppeur) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.DEVELOPPEUR_PAR_ID);
		ps.setInt(1, idDeveloppeur);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Developpeur developpeur = new Developpeur();
			developpeur.setIdDeveloppeur(rs.getInt(1));
			developpeur.setNomDeveloppeur(rs.getString(2));
			developpeur.setPrenomDeveloppeur(rs.getString(3));
			developpeur.setMotDePasse(rs.getString(4));
			developpeur.setEstAdmin(rs.getBoolean(5));

			return developpeur;
		}
		return null;
	}

	@Override
	public void delete(int idDeveloppeur) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.SUPPRIMER_DEVELOPPEUR);
		statement.setInt(1, idDeveloppeur);
		statement.execute();

	}

	@Override
	public Developpeur update(Developpeur developpeur) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.MODIFIER_DEVELOPPEUR);
		statement.setString(1, developpeur.getNomDeveloppeur());
		statement.setString(2, developpeur.getPrenomDeveloppeur());
		statement.setString(3, developpeur.getMotDePasse());
		statement.setBoolean(4, developpeur.isEstAdmin());

		statement.setInt(5, developpeur.getIdDeveloppeur());
		statement.execute();
		return developpeur;
	}

	@Override
	public Developpeur findDeveloppeurByNomPrenomMotDePasse(String nom, String prenom, String motDePasse) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.DEVELOPPEUR_PAR_NOM_PRENOM_MOT_DE_PASSE);
		ps.setString(1, nom);
		ps.setString(2, prenom);
		ps.setString(3, motDePasse);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Developpeur developpeur = new Developpeur();
			developpeur.setIdDeveloppeur(rs.getInt(1));
			developpeur.setNomDeveloppeur(rs.getString(2));
			developpeur.setPrenomDeveloppeur(rs.getString(3));
			developpeur.setMotDePasse(rs.getString(4));
			developpeur.setEstAdmin(rs.getBoolean(5));

			return developpeur;
		}
		return null;
	}

	@Override
	public Developpeur findByNomAndMotDePasse(String nom, String mdp) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.DEVELOPPEUR_PAR_NOM_MOT_DE_PASSE,Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, nom);
		ps.setString(2, mdp);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Developpeur developpeur = new Developpeur();
			developpeur.setIdDeveloppeur(rs.getInt(1));
			developpeur.setNomDeveloppeur(rs.getString(2));
			developpeur.setPrenomDeveloppeur(rs.getString(3));
			developpeur.setMotDePasse(rs.getString(4));
			developpeur.setEstAdmin(rs.getInt(5)==1?true:false);
			return developpeur;
		}
		return null;
	}
}
