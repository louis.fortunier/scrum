package fr.cfai.scrum.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.dao.ConnexionBdd;
import fr.cfai.scrum.dao.EtatDao;
import fr.cfai.scrum.dao.Requetes;

public class EtatDaoImpl implements EtatDao {

	private Connection connection;

	public EtatDaoImpl() {
		try {
			this.connection = ConnexionBdd.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public Etat create(Etat etat) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_ETAT,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, etat.getLibelle());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		etat.setIdEtat(rs.getInt(1));
		return etat;

	}

	@Override
	public List<Etat> findAll() throws SQLException {
		// Creation d'une nouvelle liste d'etats
		List<Etat> etats = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(Requetes.TOUS_LES_ETAT);
		while (rs.next()) {
			Etat etat = new Etat();
			etat.setIdEtat(rs.getInt(1));
			etat.setLibelle(rs.getString(2));
			etats.add(etat);
		}
		return etats;
	}

	@Override
	public Etat findEtatById(int idEtat) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.ETAT_PAR_ID);
		ps.setInt(1,idEtat);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			Etat etat = new Etat();
			etat.setIdEtat(rs.getInt(1));
			etat.setLibelle(rs.getString(2));
			return etat;
			}
		return null;
	}

	@Override
	public void delete(int idEtat) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.SUPPRIMER_ETAT);
		statement.setInt(1, idEtat);
		statement.execute();		
	}

	@Override
	public Etat update(Etat etat) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.MODIFIER_ETAT);
		statement.setString(1,etat.getLibelle());
		statement.setInt(2,etat.getIdEtat());
		statement.execute();
		return etat;
	}

	@Override
	public Etat findEtatByIntitule(String libelle) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.ETAT_PAR_INTITULE);
		ps.setString(1, libelle);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Etat etat = new Etat();
			etat.setIdEtat(rs.getInt(1));
			etat.setLibelle(rs.getString(2));
			return etat;
		}
		return null;
	}

}
