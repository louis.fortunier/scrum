package fr.cfai.scrum.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cfai.scrum.business.Sprint;
import fr.cfai.scrum.dao.ConnexionBdd;
import fr.cfai.scrum.dao.Requetes;
import fr.cfai.scrum.dao.SprintDao;

public class SprintDaoImpl implements SprintDao {

	private Connection connection;

	public SprintDaoImpl() {
		try {
			this.connection = ConnexionBdd.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	@Override
	public Sprint create(Sprint sprint) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_SPRINT,
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, sprint.getNom());
		ps.setDate(2, new java.sql.Date(sprint.getDateDebut().getTime()));
		ps.setDate(3, new java.sql.Date(sprint.getDateFin().getTime()));
		ps.setInt(4, sprint.isActive()?1:0);
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		rs.next();
		sprint.setIdSprint(rs.getInt(1));
		return sprint;
	}

	@Override
	public List<Sprint> findAll() throws SQLException {
		List<Sprint> sprints = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(Requetes.TOUS_LES_SPRINTS);
		while (rs.next()) {
			Sprint sprint = new Sprint();
			sprint.setIdSprint(rs.getInt(1));
			sprint.setNom(rs.getString(2));
			sprint.setDateDebut(new java.util.Date(rs.getDate(3).getTime()));
			sprint.setDateFin(new java.util.Date(rs.getDate(4).getTime()));
			sprint.setActive(rs.getInt(5)== 1?true:false);
			sprints.add(sprint);
		}
		return sprints;
	}

	@Override
	public Sprint findSprintById(int idSprint) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.SPRINT_PAR_ID);
		ps.setInt(1,idSprint);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			Sprint sprint = new Sprint();
			sprint.setIdSprint(rs.getInt(1));
			sprint.setNom(rs.getString(2));
			sprint.setDateDebut(new java.util.Date(rs.getDate(3).getTime()));
			sprint.setDateFin(new java.util.Date(rs.getDate(4).getTime()));
			sprint.setActive(rs.getInt(5)== 1?true:false);
			return sprint;
			}
		return null;
	}
	
	@Override
	public Sprint findSprintByNom(String nom) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.SPRINT_PAR_NOM);
		ps.setString(1,nom);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			Sprint sprint = new Sprint();
			sprint.setIdSprint(rs.getInt(1));
			sprint.setNom(rs.getString(2));
			sprint.setDateDebut(new java.util.Date(rs.getDate(3).getTime()));
			sprint.setDateFin(new java.util.Date(rs.getDate(4).getTime()));
			sprint.setActive(rs.getInt(5)== 1?true:false);
			return sprint;
			}
		return null;
	}
	
	public Sprint findSprintActif() throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.SPRINT_ACTIF);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			Sprint sprint = new Sprint();
			sprint.setIdSprint(rs.getInt(1));
			sprint.setNom(rs.getString(2));
			sprint.setDateDebut(new java.util.Date(rs.getDate(3).getTime()));
			sprint.setDateFin(new java.util.Date(rs.getDate(4).getTime()));
			sprint.setActive(rs.getInt(5)== 1?true:false);
			return sprint;
			}
		return null;
	}

	@Override
	public void delete(int idSprint) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.SUPPRIMER_SPRINT);
		statement.setInt(1, idSprint);
		statement.execute();
	}

	@Override
	public Sprint update(Sprint sprint) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.MODIFIER_SPRINT);
		statement.setString(1,sprint.getNom());
		statement.setDate(2, new java.sql.Date(sprint.getDateDebut().getTime()));
		statement.setDate(3, new java.sql.Date(sprint.getDateFin().getTime()));
		statement.setInt(4, sprint.isActive()?1:0);
		statement.setInt(5,sprint.getIdSprint());
		statement.execute();
		return sprint;
	}

}
