package fr.cfai.scrum.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.cfai.scrum.business.Tache;
import fr.cfai.scrum.dao.ConnexionBdd;
import fr.cfai.scrum.dao.DeveloppeurDao;
import fr.cfai.scrum.dao.EtatDao;
import fr.cfai.scrum.dao.Requetes;
import fr.cfai.scrum.dao.SprintDao;
import fr.cfai.scrum.dao.TacheDao;
import fr.cfai.scrum.dao.TypeDao;

public class TacheDaoImpl implements TacheDao {

	private Connection connection;
	private TypeDao td;
	private SprintDao sd;
	private DeveloppeurDao dd;
	private EtatDao ed;

	public TacheDaoImpl() {

		td = new TypeDaoImpl();
		sd = new SprintDaoImpl();
		dd = new DeveloppeurDaoImpl();
		ed = new EtatDaoImpl();
		try {
			this.connection = ConnexionBdd.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public Tache create(Tache tache) throws SQLException {
		if (tache.getDeveloppeur() != null) {
			PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_TACHE, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, tache.getIntitule());
			ps.setInt(2, tache.getType().getIdType());
			ps.setInt(3, tache.getSprint().getIdSprint());
			ps.setInt(4, tache.getDeveloppeur().getIdDeveloppeur());
			ps.setInt(5, tache.getEtat().getIdEtat());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			tache.setIdTache(rs.getInt(1));
		} else {
			PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_TACHE_SANS_DEVELOPPEUR, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, tache.getIntitule());
			ps.setInt(2, tache.getType().getIdType());
			ps.setInt(3, tache.getSprint().getIdSprint());
			ps.setInt(4, tache.getEtat().getIdEtat());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			tache.setIdTache(rs.getInt(1));
		}
		return tache;

	}

	@Override
	public List<Tache> findAll() throws SQLException {
		// Creation d'une nouvelle liste d'etats
		List<Tache> taches = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(Requetes.TOUTES_LES_TACHES);
		while (rs.next()) {
			Tache tache = new Tache();
			tache.setIdTache(rs.getInt(1));
			tache.setIntitule(rs.getString(2));
			tache.setType(td.findTypeById(rs.getInt(3)));
			tache.setSprint(sd.findSprintById(rs.getInt(4)));
			tache.setDeveloppeur(dd.findDeveloppeurById(rs.getInt(5)));
			tache.setEtat(ed.findEtatById(rs.getInt(6)));
			taches.add(tache);
		}
		return taches;
	}

	@Override
	public Tache findTacheById(int idTache) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.TACHE_PAR_ID);
		ps.setInt(1, idTache);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Tache tache = new Tache();
			tache.setIdTache(rs.getInt(1));
			tache.setIntitule(rs.getString(2));
			tache.setType(td.findTypeById(rs.getInt(3)));
			tache.setSprint(sd.findSprintById(rs.getInt(4)));
			tache.setDeveloppeur(dd.findDeveloppeurById(rs.getInt(5)));
			tache.setEtat(ed.findEtatById(rs.getInt(6)));
			return tache;
		}
		return null;
	}

	@Override
	public void delete(int idTache) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.SUPPRIMER_TACHE);
		statement.setInt(1, idTache);
		statement.execute();

	}

	@Override
	public Tache update(Tache tache) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.MODIFIER_TACHE);
		statement.setString(1, tache.getIntitule());
		statement.setInt(2, tache.getType().getIdType());
		statement.setInt(3, tache.getSprint().getIdSprint());
		statement.setInt(4, tache.getDeveloppeur().getIdDeveloppeur());
		statement.setInt(5, tache.getIdTache());
		statement.setInt(6, tache.getEtat().getIdEtat());
		statement.execute();

		return tache;
	}

	@Override
	public void updateEtat(int idTache, int idEtat) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.MODIFIER_ETAT_TACHE);
		statement.setInt(1, idEtat);
		statement.setInt(2, idTache);
		statement.execute();
	}

}
