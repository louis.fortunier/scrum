package fr.cfai.scrum.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.cfai.scrum.business.ChangementEtat;
import fr.cfai.scrum.dao.ChangementEtatDao;
import fr.cfai.scrum.dao.ConnexionBdd;
import fr.cfai.scrum.dao.EtatDao;
import fr.cfai.scrum.dao.Requetes;
import fr.cfai.scrum.dao.TacheDao;

public class ChangementEtatDaoImpl implements ChangementEtatDao {

	private Connection connection;
	private EtatDao ed;
	private TacheDao td;

	public ChangementEtatDaoImpl() {
		try {
			this.connection = ConnexionBdd.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}

		ed = new EtatDaoImpl();
		td = new TacheDaoImpl();
	}

	@Override
	public ChangementEtat create(ChangementEtat changementEtat) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.AJOUT_CHANGEMENT_ETAT,
				Statement.RETURN_GENERATED_KEYS);
		ps.setTimestamp(1, new Timestamp(changementEtat.getDateDebut().getTime()));
		ps.setInt(2, changementEtat.getEtat().getIdEtat());
		ps.setInt(3, changementEtat.getTache().getIdTache());
		ps.executeUpdate();
		return changementEtat;
	}

	@Override
	public List<ChangementEtat> findAll() throws SQLException {
		List<ChangementEtat> changementsEtat = new ArrayList<>();
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(Requetes.TOUS_LES_CHANGEMENT_ETAT);
		while (rs.next()) {
			ChangementEtat changementEtat = new ChangementEtat();
			changementEtat.setDateDebut(new java.util.Date(rs.getTimestamp(4).getTime()));
			changementEtat.setDateFin(new java.util.Date(rs.getTimestamp(3).getTime()));
			changementEtat.setEtat(ed.findEtatById(rs.getInt(1)));
			changementEtat.setTache(td.findTacheById(rs.getInt(2)));
			changementsEtat.add(changementEtat);
		}
		return changementsEtat;
	}

	@Override
	public List<ChangementEtat> findChangementEtatByTacheId(int idTache) throws SQLException {
		List<ChangementEtat> changementsEtat = new ArrayList<>();
		PreparedStatement ps = connection.prepareStatement(Requetes.CHANGEMENT_ETAT_PAR_ID);
		ps.setInt(1, idTache);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ChangementEtat changementEtat = new ChangementEtat();
			changementEtat.setDateDebut(new java.util.Date(rs.getTimestamp(3).getTime()));
			if (rs.getDate(4) == null) {
				changementEtat.setDateFin(null);
			} else {
				changementEtat.setDateFin(new java.util.Date(rs.getTimestamp(4).getTime()));
			}
			changementEtat.setEtat(ed.findEtatById(rs.getInt(1)));
			changementEtat.setTache(td.findTacheById(rs.getInt(2)));
			changementsEtat.add(changementEtat);
		}
		return changementsEtat;
	}

	@Override
	public ChangementEtat findChangementEtatByDateFinNullAndTacheId(int idTache) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(Requetes.CHANGEMENT_ETAT_PAR_DATE_FIN_NULL_ET_TACHE_ID);
		ps.setInt(1, idTache);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ChangementEtat changementEtat = new ChangementEtat();
			changementEtat.setDateDebut(new java.util.Date(rs.getTimestamp(3).getTime()));
			if (rs.getDate(4) == null) {
				changementEtat.setDateFin(null);
			} else {
				changementEtat.setDateFin(new java.util.Date(rs.getTimestamp(4).getTime()));
			}
			changementEtat.setEtat(ed.findEtatById(rs.getInt(1)));
			changementEtat.setTache(td.findTacheById(rs.getInt(2)));
			return changementEtat;
		}
		return null;
	}

	@Override
	public void delete(int idChangementEtat) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.SUPPRIMER_CHANGEMENT_ETAT);
		statement.setInt(1, idChangementEtat);
		statement.execute();

	}

	@Override
	public ChangementEtat update(ChangementEtat changementEtat) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(Requetes.MODIFIER_CHANGEMENT_ETAT);
		statement.setTimestamp(1, new Timestamp(changementEtat.getDateDebut().getTime()));
		statement.setTimestamp(2, new Timestamp(changementEtat.getDateFin().getTime()));
		statement.setInt(3, changementEtat.getEtat().getIdEtat());
		statement.setInt(4, changementEtat.getTache().getIdTache());
		statement.setInt(5, changementEtat.getEtat().getIdEtat());
		statement.setInt(6, changementEtat.getTache().getIdTache());
		statement.setTimestamp(7, new Timestamp(changementEtat.getDateDebut().getTime()));
		statement.execute();
		return changementEtat;
	}

}
