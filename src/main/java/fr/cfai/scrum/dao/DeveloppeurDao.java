package fr.cfai.scrum.dao;

import java.sql.SQLException;
import java.util.List;

import fr.cfai.scrum.business.Developpeur;

public interface DeveloppeurDao {
	public Developpeur create(Developpeur developpeur) throws SQLException;

	public List<Developpeur> findAll() throws SQLException;

	public Developpeur findDeveloppeurById(int idDeveloppeur) throws SQLException;

	public Developpeur findDeveloppeurByNomPrenomMotDePasse(String nom, String prenom, String motDePasse)throws SQLException;

	public void delete(int idDeveloppeur) throws SQLException;

	public Developpeur update(Developpeur developpeur) throws SQLException;
	
	public Developpeur findByNomAndMotDePasse(String nom,String mdp) throws SQLException;
}
