package fr.cfai.scrum.dao;

public class Requetes {

	public static final String AJOUT_ETAT = "INSERT INTO etat(libelle) VALUE(?)";
	public static final String TOUS_LES_ETAT = "SELECT * FROM etat";
	public static final String ETAT_PAR_ID = "SELECT * FROM etat WHERE idEtat = ?";
	public static final String SUPPRIMER_ETAT = "DELETE FROM etat WHERE idEtat = ?";
	public static final String MODIFIER_ETAT = "UPDATE etat SET libelle = ? WHERE idEtat = ?";
	public static final String ETAT_PAR_INTITULE = "SELECT * FROM etat WHERE libelle = ?";

	public static final String AJOUT_DEVELOPPEUR = "INSERT INTO developpeur(nom,prenom,motDePasse,estAdmin) VALUE(?,?,?,?)";
	public static final String TOUS_LES_DEVELOPPEURS = "SELECT * FROM developpeur";
	public static final String DEVELOPPEUR_PAR_ID = "SELECT * FROM developpeur WHERE idDeveloppeur = ?";
	public static final String DEVELOPPEUR_PAR_NOM_PRENOM_MOT_DE_PASSE = "SELECT * FROM developpeur WHERE nom = ? AND prenom = ? AND motDePasse = ?";
	public static final String SUPPRIMER_DEVELOPPEUR = "DELETE FROM developpeur WHERE idDeveloppeur = ?";
	public static final String MODIFIER_DEVELOPPEUR = "UPDATE developpeur SET nom = ?, prenom = ?,motDePasse = ?, estAdmin = ? WHERE idDeveloppeur = ?";
	public static final String DEVELOPPEUR_PAR_NOM_MOT_DE_PASSE = "SELECT * FROM developpeur WHERE nom=? AND motDePasse=?";

	public static final String AJOUT_TYPE = "INSERT INTO type(intitule,couleur) VALUE(?,?)";
	public static final String TOUS_LES_TYPES = "SELECT * FROM type";
	public static final String TYPE_PAR_ID = "SELECT * FROM type WHERE idType = ?";
	public static final String SUPPRIMER_TYPE = "DELETE FROM type WHERE idType = ?";
	public static final String MODIFIER_TYPE = "UPDATE type SET intitule = ?, couleur = ? WHERE idType = ?";
	public static final String TYPE_PAR_NOM_COULEUR = "SELECT * FROM type WHERE intitule = ? AND couleur = ?";

	public static final String AJOUT_SPRINT = "INSERT INTO sprint(nom,dateDebut,dateFin,isActive) VALUE(?,?,?,?)";
	public static final String TOUS_LES_SPRINTS = "SELECT * FROM sprint";
	public static final String SPRINT_PAR_ID = "SELECT * FROM sprint WHERE idSprint = ?";
	public static final String SPRINT_PAR_NOM = "SELECT * FROM sprint WHERE nom = ?";
	public static final String SPRINT_ACTIF = "SELECT * FROM sprint WHERE isActive = 1";
	public static final String SUPPRIMER_SPRINT = "DELETE FROM sprint WHERE idSprint = ?";
	public static final String MODIFIER_SPRINT = "UPDATE sprint SET nom = ?, dateDebut = ?, dateFin = ?,isActive = ? WHERE idSprint = ?";

	public static final String AJOUT_TACHE = "INSERT INTO tache(intitule,idType,idSprint,idDeveloppeur,idEtat) VALUE(?,?,?,?,?)";
	public static final String AJOUT_TACHE_SANS_DEVELOPPEUR = "INSERT INTO tache(intitule,idType,idSprint,idEtat) VALUE(?,?,?,?)";
	public static final String TOUTES_LES_TACHES = "SELECT * FROM tache";
	public static final String TACHE_PAR_ID = "SELECT * FROM tache WHERE idTache = ?";
	public static final String SUPPRIMER_TACHE = "DELETE FROM tache WHERE idTache = ?";
	public static final String MODIFIER_TACHE = "UPDATE tache SET intitule = ?, idType = ?, idSprint = ?,idDeveloppeur = ? WHERE idTache = ?";
	public static final String MODIFIER_ETAT_TACHE = "UPDATE tache SET idEtat = ? WHERE idTache = ?";

	public static final String AJOUT_CHANGEMENT_ETAT = "INSERT INTO changer(dateDebut,idEtat,idTache) VALUE(?,?,?)";
	public static final String TOUS_LES_CHANGEMENT_ETAT = "SELECT * FROM changer";
	public static final String CHANGEMENT_ETAT_PAR_ID = "SELECT * FROM changer WHERE idTache = ?";
	public static final String CHANGEMENT_ETAT_PAR_DATE_FIN_NULL_ET_TACHE_ID = "SELECT * FROM changer WHERE idTache = ? AND dateFin is null";
	public static final String SUPPRIMER_CHANGEMENT_ETAT = "DELETE FROM changer WHERE idChangementEtat = ?";
	public static final String MODIFIER_CHANGEMENT_ETAT = "UPDATE changer SET dateDebut = ?,dateFin = ?,idEtat = ?,idTache = ? WHERE idEtat = ? AND idTache = ? AND dateDebut = ? ";

}
