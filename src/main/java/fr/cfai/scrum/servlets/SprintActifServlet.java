package fr.cfai.scrum.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cfai.scrum.business.Sprint;
import fr.cfai.scrum.service.SprintService;
import fr.cfai.scrum.service.impl.SprintServiceImpl;

/**
 * Servlet implementation class SprintActifServlet
 */
@WebServlet("/SprintActifServlet")
public class SprintActifServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private SprintService sprintService;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SprintActifServlet() {
        super();
        sprintService = new SprintServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		String nouveauSprintNom = request.getParameter("nouveauSprintNom");
		Date nouveauSprintDateDebut = null;
		Date nouveauSprintDateFin = null;
		try {
			nouveauSprintDateDebut = formatter.parse(request.getParameter("nouveauSprintDateDebut"));
			nouveauSprintDateFin = formatter.parse(request.getParameter("nouveauSprintDateFin"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		sprintService.ajouterSprint(nouveauSprintNom, nouveauSprintDateDebut, nouveauSprintDateFin, false);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idSprintActif = Integer.parseInt(request.getParameter("idSprintActif"));
		Sprint sprintActif = sprintService.recupererSprintActif();
		Sprint futureSprintActif = sprintService.recupererSprintParId(idSprintActif);
		sprintActif.setActive(false);
		futureSprintActif.setActive(true);
		sprintService.modifierSprint(sprintActif);
		sprintService.modifierSprint(futureSprintActif);
	}

}
