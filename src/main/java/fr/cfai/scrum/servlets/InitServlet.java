package fr.cfai.scrum.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.business.Sprint;
import fr.cfai.scrum.business.Tache;
import fr.cfai.scrum.business.Type;
import fr.cfai.scrum.service.DeveloppeurService;
import fr.cfai.scrum.service.EtatService;
import fr.cfai.scrum.service.SprintService;
import fr.cfai.scrum.service.TacheService;
import fr.cfai.scrum.service.TypeService;
import fr.cfai.scrum.service.impl.DeveloppeurServiceImpl;
import fr.cfai.scrum.service.impl.EtatServiceImpl;
import fr.cfai.scrum.service.impl.SprintServiceImpl;
import fr.cfai.scrum.service.impl.TacheServiceImpl;
import fr.cfai.scrum.service.impl.TypeServiceImpl;

@WebServlet(urlPatterns = { "/" }, loadOnStartup = 1)
public class InitServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public InitServlet() {
		
		initDeveloppeur();
		initEtat();
		initType();
		initSprint();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("connexion.jsp").forward(request, response);
	}
	
	private void initDeveloppeur() {
		DeveloppeurService developpeurService = new DeveloppeurServiceImpl();
		Developpeur dev = developpeurService.verifierDeveloppeur("admin", "admin", "12345");
		if (dev == null) {
			developpeurService.ajouterDeveloppeur("admin", "admin", "12345", true);
		}
			
		System.out.println("Les developpeurs administrateurs a été crée");
	}
	
	private void initEtat() {
		EtatService etatService = new EtatServiceImpl();
		Etat etat = etatService.verifierEtat("To do");
		if (etat == null) {
			etatService.ajouterEtat("To do");
		}
		etat = etatService.verifierEtat("In progress");
		if (etat == null) {
			etatService.ajouterEtat("In progress");
		}
		etat = etatService.verifierEtat("To verify");
		if (etat == null) {
			etatService.ajouterEtat("To verify");
		}
		etat = etatService.verifierEtat("Done");
		if (etat == null) {
			etatService.ajouterEtat("Done");
		}
		
		System.out.println("Les états ont été créer");
	}
	
	private void initType() {
		TypeService typeService = new TypeServiceImpl();
		Type typeCarnetDeProduit = typeService.recupererTypeParNomCouleur("carnet de produit", "blue");
		if (typeCarnetDeProduit == null) {
			typeService.ajouterType("carnet de produit", "blue");
		}
		
		Type typeBugs = typeService.recupererTypeParNomCouleur("bugs", "orange");
		if (typeBugs == null) {
			typeService.ajouterType("bugs", "orange");
		}
		
		Type typeAmelioration = typeService.recupererTypeParNomCouleur("amélioration", "green");
		if (typeAmelioration == null) {
			typeService.ajouterType("amélioration", "green");
		}
		
		Type typeSpikes = typeService.recupererTypeParNomCouleur("spikes", "red");
		if (typeSpikes == null) {
			typeService.ajouterType("spikes", "red");
		}
		
		System.out.println("Les types ont été créer");
	}
	
	private void initSprint() {
		SprintService sprintService = new SprintServiceImpl();
		Sprint sprint = sprintService.recupererSprintParNom("Sprint");
		if (sprint == null) {
			sprintService.ajouterSprint("Sprint", new Date(), new Date(), true);
		}
	}
}
