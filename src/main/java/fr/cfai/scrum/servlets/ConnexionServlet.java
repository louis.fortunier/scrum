package fr.cfai.scrum.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.service.DeveloppeurService;
import fr.cfai.scrum.service.impl.DeveloppeurServiceImpl;

/**
 * Servlet implementation class ConnexionServlet
 */
@WebServlet("/ConnexionServlet")
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DeveloppeurService ds = new DeveloppeurServiceImpl();

	public ConnexionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("connexion.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// On traite la demande de connexion en provenance du navigateur internet
		String nom = request.getParameter("NOM");
		String mdp = request.getParameter("MDP");
		Developpeur developpeur = ds.recupererDeveloppeurParNomEtMotDePasse(nom, mdp);
		HttpSession session = request.getSession();
		session.setAttribute("developpeur", developpeur);
		response.sendRedirect("TableauServlet");
	}

}
