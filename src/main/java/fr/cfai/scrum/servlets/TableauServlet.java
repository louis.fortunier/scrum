package fr.cfai.scrum.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.cfai.scrum.business.Developpeur;
import fr.cfai.scrum.business.Etat;
import fr.cfai.scrum.business.Sprint;
import fr.cfai.scrum.business.Tache;
import fr.cfai.scrum.business.Type;
import fr.cfai.scrum.service.ChangementEtatService;
import fr.cfai.scrum.service.DeveloppeurService;
import fr.cfai.scrum.service.EtatService;
import fr.cfai.scrum.service.SprintService;
import fr.cfai.scrum.service.TacheService;
import fr.cfai.scrum.service.TypeService;
import fr.cfai.scrum.service.impl.ChangementEtatSreviceImpl;
import fr.cfai.scrum.service.impl.DeveloppeurServiceImpl;
import fr.cfai.scrum.service.impl.EtatServiceImpl;
import fr.cfai.scrum.service.impl.SprintServiceImpl;
import fr.cfai.scrum.service.impl.TacheServiceImpl;
import fr.cfai.scrum.service.impl.TypeServiceImpl;

/**
 * Servlet implementation class TableauServlet
 */
@WebServlet("/TableauServlet")
public class TableauServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private EtatService etatService;
	private TacheService tacheService;
	private SprintService sprintService;
	private TypeService typeService;
	private DeveloppeurService developpeurService;
	private ChangementEtatService changementEtatService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TableauServlet() {
		etatService = new EtatServiceImpl();
		tacheService = new TacheServiceImpl();
		sprintService = new SprintServiceImpl();
		typeService = new TypeServiceImpl();
		developpeurService = new DeveloppeurServiceImpl();
		changementEtatService = new ChangementEtatSreviceImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getSession().getAttribute("developpeur") == null) {
			response.sendRedirect("ConnexionServlet");
		} else {

			List<Sprint> sprints = sprintService.recupererSprint();
			List<Etat> etats = etatService.recupererEtat();
			List<Tache> taches = tacheService.recupererTaches();
			Sprint sprint = sprintService.recupererSprintActif();
			List<Type> types = typeService.recupererType();
			List<Developpeur> developpeurs = developpeurService.recupererDeveloppeurs();

			request.setAttribute("etats", etats);
			request.setAttribute("taches", taches);
			request.setAttribute("sprints", sprints);
			request.setAttribute("sprint", sprint);
			request.setAttribute("types", types);
			request.setAttribute("developpeurs", developpeurs);

			request.getRequestDispatcher("tableau.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		SprintService sprintService = new SprintServiceImpl();
		DeveloppeurService devService = new DeveloppeurServiceImpl();
		EtatService etatService = new EtatServiceImpl();
		TypeService typeService = new TypeServiceImpl();

		int id = Integer.parseInt(request.getParameter("nouvelleTacheSprint"));
		int idType = Integer.parseInt(request.getParameter("nouvelleTacheType"));
		int idDev = Integer.parseInt(request.getParameter("nouvelleTacheDeveloppeur"));

		Tache tache = tacheService.ajouterTache(request.getParameter("nouvelleTache"), typeService.recupererTypeParId(idType),
				sprintService.recupererSprintParId(id), devService.recupererDeveloppeurParId(idDev),
				etatService.verifierEtat("To do"));
		
		changementEtatService.ajouterChangementEtat(new Date(), etatService.verifierEtat("To do"),
				tache);
	}

}
