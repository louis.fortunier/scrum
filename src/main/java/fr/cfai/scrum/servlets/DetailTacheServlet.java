package fr.cfai.scrum.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import fr.cfai.scrum.business.ChangementEtat;
import fr.cfai.scrum.business.Tache;
import fr.cfai.scrum.service.ChangementEtatService;
import fr.cfai.scrum.service.TacheService;
import fr.cfai.scrum.service.impl.ChangementEtatSreviceImpl;
import fr.cfai.scrum.service.impl.TacheServiceImpl;

/**
 * Servlet implementation class DetailTacheServlet
 */
@WebServlet("/DetailTacheServlet")
public class DetailTacheServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private TacheService tacheService;
	private ChangementEtatService changementEtatService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DetailTacheServlet() {
		super();
		tacheService = new TacheServiceImpl();
		changementEtatService = new ChangementEtatSreviceImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idTacheAModifier = Integer.parseInt(request.getParameter("idTacheAModifier"));

		Tache tache = tacheService.recupererTacheParId(idTacheAModifier);
		
		List<ChangementEtat> changementsEtat = changementEtatService.recupererChangementEtatParTacheId(idTacheAModifier);

		Collections.sort(changementsEtat, new Comparator<ChangementEtat>() {
		    @Override
		    public int compare(ChangementEtat ce1, ChangementEtat ce2) {
		        return ce1.getDateDebut().compareTo(ce2.getDateDebut());
		    }
		});
		
		tache.setChangementsEtats(changementsEtat);
		
		Gson gson = new Gson();
		String json = gson.toJson(tache);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json);
	}

}
