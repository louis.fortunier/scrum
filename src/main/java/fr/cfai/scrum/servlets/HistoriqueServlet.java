package fr.cfai.scrum.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cfai.scrum.business.ChangementEtat;
import fr.cfai.scrum.service.ChangementEtatService;
import fr.cfai.scrum.service.EtatService;
import fr.cfai.scrum.service.TacheService;
import fr.cfai.scrum.service.impl.ChangementEtatSreviceImpl;
import fr.cfai.scrum.service.impl.EtatServiceImpl;
import fr.cfai.scrum.service.impl.TacheServiceImpl;

/**
 * Servlet implementation class HistoriqueServlet
 */
@WebServlet("/HistoriqueServlet")
public class HistoriqueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private TacheService tacheService;
	private EtatService etatService;
	private ChangementEtatService changementEtatService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HistoriqueServlet() {
		super();
		tacheService = new TacheServiceImpl();
		etatService = new EtatServiceImpl();
		changementEtatService = new ChangementEtatSreviceImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idTache = Integer.parseInt(request.getParameter("tacheId"));
		int idEtat = Integer.parseInt(request.getParameter("etatId"));

		// On recupere le changements d'état qui a une date de fin null
		ChangementEtat changementEtat = changementEtatService.recupererChangementEtatParDateFinNullEtTacheId(idTache);

		tacheService.modifierEtatTache(idTache, idEtat);

		changementEtatService.ajouterChangementEtat(new Date(), etatService.recupererEtatParId(idEtat),
				tacheService.recupererTacheParId(idTache));

	}

}
