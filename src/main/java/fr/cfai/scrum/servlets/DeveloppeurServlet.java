package fr.cfai.scrum.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cfai.scrum.service.DeveloppeurService;
import fr.cfai.scrum.service.impl.DeveloppeurServiceImpl;

/**
 * Servlet implementation class DeveloppeurServlet
 */
@WebServlet("/DeveloppeurServlet")
public class DeveloppeurServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private DeveloppeurService developpeurService;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeveloppeurServlet() {
        super();
        developpeurService = new DeveloppeurServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter("nouveauDeveloppeurNom");
		String prenom = request.getParameter("nouveauDeveloppeurPrenom");
		String motDePasse = request.getParameter("nouveauDeveloppeurMdp");
		boolean isAdmin = request.getParameter("nouveauDeveloppeurisAdministrateur").equals("1")? true : false;
		
		developpeurService.ajouterDeveloppeur(nom, prenom, motDePasse, isAdmin);
	}

}
