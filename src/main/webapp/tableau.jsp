<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<title>Scrum</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<style>
.cm {
	color: white;
	text-align: center;
	background-color: #970e00;
	border: 1px solid white;
	height: 65px;
}

.dropper {
	margin: 0px;
	width: 100%;
	height: 100%;
	background-color: #9e9e9e;
	border: 1px solid #111;
	-moz-border-radius: 10px;
	border-radius: 0px;
	-moz-transition: all 200ms linear;
	-webkit-transition: all 200ms linear;
	-o-transition: all 200ms linear;
	transition: all 200ms linear;
}

.drop_hover {
	-moz-box-shadow: 0 0 30px rgba(0, 0, 0, 0.8) inset;
	box-shadow: 0 0 30px rgba(0, 0, 0, 0.8) inset;
}

.tache {
	display: inline-block;
	margin: 5%;
	width: 200px;
	padding-top: 20px;
	color: #3D110F;
	border: 2px solid #3D110F;
	border-radius: 5px;
	text-align: center;
	font-size: 2em;
	cursor: move;
	word-wrap: break-word;
	-moz-transition: all 200ms linear;
	-webkit-transition: all 200ms linear;
	-o-transition: all 200ms linear;
	transition: all 200ms linear;
	-moz-user-select: none;
	-khtml-user-select: none;
	-webkit-user-select: none;
	user-select: none;
	padding-top: 20px;
}

.header {
	margin-top: 50px;
	margin-bottom: 50px;
}
</style>
</head>

<body>

	<header class="p-3 mb-2 bg-dark text-white">
		<div class="row header">
			<div class="col-2" id="developpeurConnecte"
				data-prodnumber="${sessionScope.developpeur}">
				<p>${ sessionScope.developpeur.prenomDeveloppeur }${ sessionScope.developpeur.nomDeveloppeur }</p>
			</div>
			<div class="col-8">
				<h1 class="text-center" id="nomSprint">${sprint.nom }</h1>
			</div>
			<div class="col-2 text-center">
				<form action="DeconnexionServlet" method="get">
					<button type="submit" class="btn btn-danger">Déconnexion</button>
				</form>
			</div>

		</div>
	</header>
	<br>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="dropdown">
					<button class="btn btn-secondary dropdown-toggle" type="button"
						id="dropdownSprint" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">Choix d'un sprint</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
						<c:forEach items="${ sprints }" var="sprint"
							varStatus="statutSprint">
							<a id="${sprint.idSprint }" class="dropdown-item sprint" href="#">${ sprint.nom }</a>
						</c:forEach>
					</div>
				</div>
			</div>
			<c:if test="${ sessionScope.developpeur.estAdmin }">
				<div class="col">
					<button class="btn btn-primary" type="button"
						data-toggle="collapse" data-target="#collapseAjoutSprint"
						aria-expanded="false" aria-controls="#collapseAjoutSprint">Ajout
						d'un sprint</button>
				</div>
				<div class="col">
					<button class="btn btn-primary" type="button"
						data-toggle="collapse" data-target="#collapseAjoutTache"
						aria-expanded="false" aria-controls="#collapseAjoutTache">Ajout
						d'une tache</button>
				</div>
				<div class="col">
					<button class="btn btn-primary" type="button"
						data-toggle="collapse" data-target="#collapseNouveauDev"
						aria-expanded="false" aria-controls="#collapseNouveauDev">Ajouter
						un developpeur</button>
				</div>
			</c:if>
		</div>
		<br>

		<!-- ajout d'un sprint -->
		<div class="collapse" id="collapseAjoutSprint">
			<div class="card card-body">
				<div class="form-group row">
					<div class="col">
						<input class="form-control" type="text" id="nouveauSprintNom"
							name="nouveauSprint" placeholder="Nom">
					</div>
					<div class="col">
						<div class="row">
							<div class="col-4">Date début</div>
							<div class="col-8">
								<input class="form-control" type="date"
									id="nouveauSprintDateDebut" name="nouveauSprint"
									placeholder="Date début">
							</div>
						</div>
					</div>
					<div class="col">
						<div class="row">
							<div class="col-4">Date fin</div>
							<div class="col-8">
								<input class="form-control" type="date"
									id="nouveauSprintDateFin" name="nouveauSprint"
									placeholder="Date Fin ex:2000-01-01">
							</div>
						</div>
					</div>
				</div>
				<input id="nouveauSprintButton" class="btn btn-primary"
					type="button" value="Ajouter">
			</div>
		</div>

		<!-- ajout d'une tache -->
		<div class="collapse" id="collapseAjoutTache">
			<div class="card card-body">
				<div class="form-group row">
					<div class="col">
						<label for="nouvelleTache">Nom de la tache</label> <input
							class="form-control" type="text" id="nouvelleTache"
							name="nouvelleTache" placeholder="Nom"> <input
							class="form-control" style="display: none" type="text"
							id="nouvelleTacheSprint" name="nouvelleTacheSprint"
							value="${sprint.idSprint }">
					</div>
					<div class="col">
						<label for="FormControlTacheType">Choix type</label> <select
							class="form-control" id="FormControlTacheType">
							<c:forEach items="${ types }" var="type" varStatus="statutTypes">
								<option value="${ type.idType }">${ type.intitule }</option>
							</c:forEach>

						</select>
					</div>
					<div class="col">
						<label for="FormControlTacheDeveloppeur">Choix developpeur</label>
						<select class="form-control" id="FormControlTacheDeveloppeur">
							<c:forEach items="${ developpeurs }" var="dev"
								varStatus="statutDeveloppeurs">
								<option value="${ dev.idDeveloppeur }">${ dev.nomDeveloppeur }
									${ dev.prenomDeveloppeur }</option>
							</c:forEach>

						</select>
					</div>
				</div>

				<input id="nouvelleTacheButton" class="btn btn-primary"
					type="button" value="Ajouter">
			</div>
		</div>

		<!-- ajout d'un dev -->
		<div class="collapse" id="collapseNouveauDev">
			<div class="card card-body">
				<div class="form-group row">
					<div class="col">
						<input class="form-control" type="text" id="nouveauDeveloppeurNom"
							name="nouveauDeveloppeurNom" placeholder="Nom">
					</div>
					<div class="col">
						<input class="form-control" type="text"
							id="nouveauDeveloppeurPrenom" name="nouveauDeveloppeurPrenom"
							placeholder="Prenom">
					</div>
					<div class="col">
						<input class="form-control" type="text" id="nouveauDeveloppeurMdp"
							name="nouveauDeveloppeurMdp" placeholder="Mot de passe">
					</div>
					<div class="col form-check">
						<input type="checkbox" class="form-check-input"
							id="nouveauDeveloppeurisAdministrateur"> <label
							class="form-check-label" for="nouveauDeveloppeurisAdministrateur">Administrateur</label>
					</div>
				</div>
				<input id="nouveauDevButton" class="btn btn-primary" type="button"
					value="Ajouter">
			</div>
		</div>

		<div class="row no-gutters">
			<c:forEach items="${ etats }" var="etat" varStatus="statutEtats">
				<div class="col">
					<div class="text-center">
						<c:out value="${ etat.libelle }" />
					</div>
					<div class="dropper text-center" id="${etat.idEtat }">
						<c:forEach items="${ taches }" var="tache"
							varStatus="statutTaches">
							<c:if
								test="${ tache.etat.idEtat eq etat.idEtat && sprint.idSprint eq tache.sprint.idSprint }">
								<c:choose>
									<c:when
										test="${ tache.developpeur.idDeveloppeur eq sessionScope.developpeur.idDeveloppeur || sessionScope.developpeur.estAdmin }">
										<c:set var="varclass" value="draggable" />
									</c:when>
									<c:otherwise>
										<c:set var="varclass" value="bad" />
									</c:otherwise>
								</c:choose>
								<div id="${ tache.idTache}" class="${varclass} tache"
									data-toggle="modal" data-target="#modalDetail"
									style="background-color: ${tache.type.couleur};">
									<c:out value="${ tache.intitule}" />
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog"
		aria-labelledby="modalDetailLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalDetailLabel"></h5>
				</div>
				<div id="bodyModal" class="modal-body">
					<h3 class="text-center">Développeur</h3>
					<p id="developpeurTache"></p>
					<h3 class="text-center">Date de création</h3>
					<p id="creationTache"></p>
					<h3 class="text-center">Historique</h3>
					<div id="histodiv" style="overflow: scroll; height: 300px"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Fermer</button>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.js"
		integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<script>
		(function() {

			var dev = document.getElementById("developpeurConnecte")
					.getAttribute("data-prodnumber");

			var dndHandler = {

				draggedElement : null, // Propriété pointant vers l'élément en cours de déplacement

				applyDragEvents : function(element) {

					element.draggable = true;

					var dndHandler = this; // Cette variable est nécessaire pour que l'événement "dragstart" ci-dessous accède facilement au namespace "dndHandler"

					element.addEventListener('dragstart', function(e) {
						dndHandler.draggedElement = e.target; // On sauvegarde l'élément en cours de déplacement
						e.dataTransfer.setData('text/plain', ''); // Nécessaire pour Firefox
					}, false);

					$.ajax({
						type : 'POST',
						url : 'HistoriqueServlet',
						data : {
							tacheId : element.id,
							etatId : element.parentElement.id
						},
						success : function() {
						}
					});

				},

				applyDropEvents : function(dropper) {

					dropper.addEventListener('dragover', function(e) {
						e.preventDefault(); // On autorise le drop d'éléments
						this.className = 'dropper text-center drop_hover'; // Et on applique le design adéquat à notre zone de drop quand un élément la survole
					}, false);

					dropper.addEventListener('dragleave', function() {
						this.className = 'dropper text-center'; // On revient au design de base lorsque l'élément quitte la zone de drop
					});

					var dndHandler = this; // Cette variable est nécessaire pour que l'événement "drop" ci-dessous accède facilement au namespace "dndHandler"

					dropper
							.addEventListener(
									'drop',
									function(e) {

										var target = e.target, draggedElement = dndHandler.draggedElement, // Récupération de l'élément concerné
										clonedElement = draggedElement
												.cloneNode(true); // On créé immédiatement le clone de cet élément

										while (target.className
												.indexOf('dropper') == -1) { // Cette boucle permet de remonter jusqu'à la zone de drop parente
											target = target.parentNode;
										}

										target.className = 'dropper text-center'; // Application du design par défaut

										clonedElement = target
												.appendChild(clonedElement); // Ajout de l'élément cloné à la zone de drop actuelle
										dndHandler
												.applyDragEvents(clonedElement); // Nouvelle application des événements qui ont été perdus lors du cloneNode()

										draggedElement.parentNode
												.removeChild(draggedElement); // Suppression de l'élément d'origine

									});

				}

			};

			var elements = document.querySelectorAll('.draggable'), elementsLen = elements.length;

			for (var i = 0; i < elementsLen; i++) {
				dndHandler.applyDragEvents(elements[i]); // Application des paramètres nécessaires aux élément déplaçables
			}

			var droppers = document.querySelectorAll('.dropper'), droppersLen = droppers.length;

			for (var i = 0; i < droppersLen; i++) {
				dndHandler.applyDropEvents(droppers[i]); // Application des événements nécessaires aux zones de drop
			}

		})();

		$("#nouvelleTacheButton").click(
				function() {
					$.ajax({
						type : 'POST',
						url : 'TableauServlet',
						data : {
							nouvelleTache : $('#nouvelleTache').val(),
							nouvelleTacheSprint : $('#nouvelleTacheSprint')
									.val(),
							nouvelleTacheType : $('#FormControlTacheType')
									.val(),
							nouvelleTacheDeveloppeur : $(
									'#FormControlTacheDeveloppeur').val()
						},
						success : function() {
							location.reload();
						}
					});
				});

		$("#nouveauSprintButton").click(
				function() {
					$.ajax({
						type : 'GET',
						url : 'SprintActifServlet',
						data : {
							nouveauSprintNom : $('#nouveauSprintNom').val(),
							nouveauSprintDateDebut : $(
									'#nouveauSprintDateDebut').val(),
							nouveauSprintDateFin : $('#nouveauSprintDateFin')
									.val()
						},
						success : function() {
							location.reload();
						}
					});
				});

		$("#nouveauDevButton")
				.click(
						function() {
							var isAdministrateur;
							if ($('#nouveauDeveloppeurisAdministrateur').isChecked) {
								isAdministrateur = 1;
							} else {
								isAdministrateur = 0;
							}
							$
									.ajax({
										type : 'POST',
										url : 'DeveloppeurServlet',
										data : {
											nouveauDeveloppeurNom : $(
													'#nouveauDeveloppeurNom')
													.val(),
											nouveauDeveloppeurPrenom : $(
													'#nouveauDeveloppeurPrenom')
													.val(),
											nouveauDeveloppeurMdp : $(
													'#nouveauDeveloppeurMdp')
													.val(),
											nouveauDeveloppeurisAdministrateur : isAdministrateur
										},
										success : function() {
											location.reload();
										}
									});
						});

		$(document)
				.ready(
						function() {
							$(document)
									.on(
											"click",
											".tache",
											function() {
												let id = $(this).attr("id");
												$
														.ajax({
															type : 'POST',
															url : 'DetailTacheServlet',
															data : {
																idTacheAModifier : id
															},
															dataType : "json",
															success : function(
																	responseText) {
																$('#histodiv')
																		.empty()
																console
																		.log(responseText);
																//Nom Tache
																$(
																		'#modalDetailLabel')
																		.empty();
																$(
																		'#modalDetailLabel')
																		.append(
																				responseText.intitule);
																//Nom developpeur tache
																$(
																		'#developpeurTache')
																		.empty();
																$(
																		'#developpeurTache')
																		.append(
																				responseText.developpeur.nom);
																$(
																		'#developpeurTache')
																		.append(
																				' ');
																$(
																		'#developpeurTache')
																		.append(
																				responseText.developpeur.prenom);
																//DAte création
																$(
																		'#creationTache')
																		.empty();
																$(
																		'#creationTache')
																		.append(
																				responseText.changementsEtats[0].dateDebut);
																// Historique

																$(
																		'#historiqueTache')
																		.append(
																				' ');
																responseText.changementsEtats
																		.forEach(function(
																				element) {

																			var bodyModal = document
																					.getElementById("histodiv");
																			var histo = document
																					.createElement("div");
																			var dateD = document
																					.createElement("p");
																			var dateF = document
																					.createElement("p");
																			var etat = document
																					.createElement("p");

																			histo.classList
																					.add("historique");
																			dateD.textContent = element.dateDebut;
																			dateF.textContent = element.dateFin;
																			etat.textContent = element.etat.libelle;

																			histo.style.borderBottom = "solid black";

																			histo
																					.appendChild(etat);
																			histo
																					.appendChild(dateD);
																			histo
																					.appendChild(dateF);
																			bodyModal
																					.appendChild(histo);

																			//$('#bodyModal').add(
																			//	"<p>" + element.dateDebut + " </p>");
																		});
															}
														});
											});

						});

		$(".sprint").click(function() {
			let id = $(this).attr("id");
			idSprint = id;
			$.ajax({
				type : 'POST',
				url : 'SprintActifServlet',
				data : {
					idSprintActif : id
				},
				success : function() {
					location.reload();
				}
			})
		});
	</script>
</body>

</html>
